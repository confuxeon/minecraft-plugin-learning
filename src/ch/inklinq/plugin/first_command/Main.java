package ch.inklinq.plugin.first_command;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        System.out.println("First Plugin is enabled!");
    }

    @Override
    public void onDisable() {
        System.out.println("First Plugin is disabled!");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equals("hello")) {
            if (sender instanceof Player) {
                Player player = (Player) sender;

                player.sendMessage(ChatColor.GRAY + "Hello, " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + ". Your health has been restored.");
                player.setHealth(20.0);
            } else {
                System.out.println("This command can not be used in console.");

            }
        }
        if (cmd.getName().equals("heal")) {
            Player player = (Player) sender;

            player.sendMessage(ChatColor.GREEN + player.getName() + ChatColor.GRAY + " you have been healed.");
            player.setHealth(20.0);
            player.setFoodLevel(40);
        }
        if (cmd.getName().equals("gmc")) {
            Player player = (Player) sender;
            player.setGameMode(GameMode.CREATIVE);
        }
        return false;
    }
}
